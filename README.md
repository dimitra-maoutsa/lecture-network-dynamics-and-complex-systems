# Lecture material for (part of) the lecture 
# Network Dynamics & Complex Systems - Theoretical and Computational Tools 
(under construction)

Lecture content created and covered by me (Dimitra Maoutsa) and provided here:
- Approximation theory (asymptotic solutions for ODEs, matched asymptotics) (9.11.2016)
- [Random networks](https://gitlab.com/di.ma/lecture-network-dynamics-and-complex-systems/-/blob/master/Lecture_16-11-16.pdf) (16.11.2016) <img src="random_networks.png" alt="random networks by dimitra maoutsa" width="30%" height="30%"> <img src="prob_connected_to_giant.png" alt="computing connection probabilities for emergence of giant component by dimitra maoutsa" width="25%" height="25%">

- [Searching in complex networks](https://gitlab.com/di.ma/lecture-network-dynamics-and-complex-systems/-/blob/master/Lecture_23-11-16.pdf) (23.11.2016) <img src="network_search.png" alt="searching in random networks by dimitra maoutsa" width="20%" height="20%">
- Random walks on graphs (14.12.2016)


Lecture took place during winter semester 2016/17 at Georg-August-Universität Göttingen.

Rest of the lecture was covered by Diemut Regel, Nahal Sharafi, and Marc Timme.

( Contect provided here was created by Dimitra Maoutsa. )

